# base-components

Base kubernetes components for Athena infrastructure.

- TODO: Convert all of this into a single base helm chart.

## Manual steps

### Nginx

This is a reverse proxy for all projects. This is required so that we only need a single load balancer for the project (they are expensive). This also terminates SSL connections.

Requirements:
- SSL certificate (`cert.pem`) and key (`cert.pem.key`) from Cloudflare.

Speak to Phil or Alex for these details.

```
export GCP_PROJECT=athena-epidemic
export NS=base
export CERT_NAME=default-tls
export CERT_FILE=cert.pem
export KEY_FILE=cert.pem.key
kubectl create ns ${NS}
kubectl create secret tls ${CERT_NAME} --key ${KEY_FILE} --cert ${CERT_FILE} --namespace ${NS}
helm install prod stable/nginx-ingress --namespace base --set controller.metrics.enabled=true --set rbac.create=true --set controller.publishService.enabled=true --set controller.extraArgs.default-ssl-certificate=${NS}/${CERT_NAME}
```

### External DNS

This is required to automatically update the DNS records in Cloudflare if the IP addresses change.

Requirements:
- Cloudflare email and API key.

Ask Phil or Alex for these details.

```
export CLOUDFLARE_API_KEY=XXXXX
export CLOUDFLARE_EMAIL=XXXXX
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install prod-dns bitnami/external-dns --namespace ${NS} --set provider=cloudflare --set cloudflare.proxied=true --set cloudflare.email=${CLOUDFLARE_EMAIL} --set cloudflare.apiToken=${CLOUDFLARE_API_KEY} --set policy=sync
```